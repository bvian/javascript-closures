function cacheFunction(cb) {
  if (typeof cb !== "function") {
    throw new Error("Parameter is not passed correctly!");
  }

  let cache = {};

  function invokeCacheFunction(...args) {
    if (cache[args] === undefined) {
      cache[args] = cb(...args);
      return cache[args];
    } else {
      return cache[args];
    }
  }

  return invokeCacheFunction;
}

module.exports = cacheFunction;
