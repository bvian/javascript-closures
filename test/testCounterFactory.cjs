const counterFactory = require("../counterFactory.cjs");

const counterObject = counterFactory();

console.log(counterObject);
console.log(counterObject.increment());
console.log(counterObject.decrement());
